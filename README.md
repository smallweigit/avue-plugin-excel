## Avue Excel导入导出插件

## Avue官网
[https://avuejs.com](https://avuejs.com)

## 介绍
- [demo](https://avuejs.com/doc/plugins/excel-plugins)
- [npm](https://www.npmjs.com/package/avue-plugin-excel)
- [git](https://gitee.com/smallweigit/avue-plugin-excel)


## 使用
```
1.安装
npm install avue-plugin-excel --save

2.导入
import $Excel from 'avue-plugin-excel'

3.使用

导出
let opt = {
  title: '文档标题',
  column: [{
    label: '复杂表头',
    prop: 'header',
    children: [
      {
        label: '姓名',
        prop: 'name'
      }, {
        label: '年龄',
        prop: 'sex'
      }
    ]
  }],
  data: [{
    name:'张三',
    sex:12
  }]
}
$Excel.excel({
  title: opt.title,
  columns: opt.column,
  data: opt.data
});


导入
//files为excel文件
$Excel.xlsx(files).then(data => {
  console.log(data.results) //导出的json数组
})

```

